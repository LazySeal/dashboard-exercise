// for vue.config.js (Vue CLI)
//var webpack = require('webpack')

module.exports = {
  chainWebpack: config => {
    config.module
      .rule("i18n")
      .resourceQuery(/blockType=i18n/)
      .use("i18n")
      .loader("@kazupon/vue-i18n-loader")
      .end();
  },
  configureWebpack: {
    devtool: "source-map"
  },
  transpileDependencies: ["vuex-module-decorators"],
  pwa: {
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "./src/sw.js",
      swDest: "service-worker.js"
    }
  }
};
const pkg = require("./package.json");

// Importing Env Variables
process.env.VUE_APP_CI_JOB_ID = process.env.CI_JOB_ID;
process.env.VUE_APP_CI_PIPELINE_ID = process.env.CI_PIPELINE_ID;
process.env.VUE_APP_CI_COMMIT_REF_NAME = process.env.CI_COMMIT_REF_NAME;
process.env.VUE_APP_CI_COMMIT_SHA = process.env.CI_COMMIT_SHA;
process.env.VUE_APP_BUILD_TIMESTAMP = new Date().toISOString();

process.env.VUE_APP_PKG_VERSION = pkg.version;

if (typeof process.env.CI_COMMIT_TAG === "undefined") {
  process.env.VUE_APP_CI_COMMIT_TAG = pkg.version;
} else {
  process.env.VUE_APP_CI_COMMIT_TAG = process.env.CI_COMMIT_TAG;
}

process.env.VUE_APP_AUTH_CONFIG = process.env.AUTH_CONFIG;
