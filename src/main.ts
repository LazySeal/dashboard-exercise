import i18n from "@/plugins/i18n";
import vuetify from "@/plugins/vuetify";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

declare global {
  interface Window {
    TheAuditCloudRemote?: Vue;
    vuexStore?: any;
  }
}

window.TheAuditCloudRemote = new Vue({
  router,
  i18n,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
