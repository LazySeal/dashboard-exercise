import VueI18n from "vue-i18n";
import Vue from "vue";

Vue.use(VueI18n);

const messages = {
  en: {
    $vuetify: {
      dataIterator: {
        rowsPerPageText: "Items per page:",
        rowsPerPageAll: "All",
        pageText: "{0}-{1} of {2}",
        noResultsText: "No matching records found",
        nextPage: "Next page",
        prevPage: "Previous page"
      },
      dataTable: {
        rowsPerPageText: "Rows per page:"
      },
      noDataText: "No data available"
    }
  },
  de: {
    $vuetify: {
      dataIterator: {
        rowsPerPageText: "Elemente pro Seite:",
        rowsPerPageAll: "Alle",
        pageText: "{0}-{1} von {2}",
        noResultsText: "Keine Elemente gefunden",
        nextPage: "Nächste Seite",
        prevPage: "Vorherige Seite"
      },
      dataTable: {
        rowsPerPageText: "Zeilen pro Seite:"
      },
      noDataText: "Keine Daten vorhanden"
    }
  }
};

const i18n = new VueI18n({
  locale: "en",
  messages: messages,
  silentTranslationWarn: true
});

export const languages = (key: string, ...params: unknown[]) =>
  i18n.t(key, params);

export default i18n;
