export enum ROUTE_NAMES {
  HOME = "home",
  DASHBOARD = "dashboard",
  DASHBOARD_DIAGRAM = "dashboard-diagram"
}
