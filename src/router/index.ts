import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import { ROUTE_NAMES } from "./routenames";
import Home from "../views/Home.vue";

// route level code-splitting
// this generates a separate chunk (audit.[hash].js) for this route
// which is lazy-loaded when the route is visited.
const Dashboard = () =>
  import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue");

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    name: ROUTE_NAMES.HOME,
    component: Home
  },
  {
    path: "/dashboard",
    name: ROUTE_NAMES.DASHBOARD,
    component: Dashboard
  },
  {
    path: "/dashboard/:diagram",
    name: ROUTE_NAMES.DASHBOARD_DIAGRAM,
    component: Dashboard
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: "is-active"
});

export default router;
