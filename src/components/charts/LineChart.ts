import { Line, mixins } from "vue-chartjs";
const { reactiveProp } = mixins;

import { Component, Prop, Watch, Vue } from "vue-property-decorator";
import { State, Getter, Action, Mutation, namespace } from "vuex-class";

@Component({
  mixins: [reactiveProp],
  extends: Line
})
export default class LineChart extends Vue {
  [x: string]: any;
  @Prop({
    type: Object
  })
  options!: any;

  @Prop({
    type: Object
  })
  chartData!: any;

  mounted() {
    // this.chartData is created in the mixin.
    // If you want to pass options please create a local options object
    this.renderChart(this.chartData, this.options);
  }
}
