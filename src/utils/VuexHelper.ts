export function mutationNs(
  api: { namespace: string | null },
  mutation: string
): string {
  return [api.namespace, mutation].filter(v => v !== null).join("/");
}

export function getterNs(
  api: { namespace: string | null },
  getter: string
): string {
  return [api.namespace, getter].filter(v => v !== null).join("/");
}

export function actionNs(
  api: { namespace: string | null },
  action: string
): string {
  return [api.namespace, action].filter(v => v !== null).join("/");
}

export function logXStored(X: string): () => void {
  return () => {
    console.log(`${X} stored in the Backend`);
  };
}
