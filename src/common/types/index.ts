export type nullable<T> = T | null;

export type NullablePartial<T> = {
  [P in keyof T]: T[P] | null;
};

export type NonEmptyArray<T> = [T, ...T[]];
