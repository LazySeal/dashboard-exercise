export interface MultilingualText {
  en: string;
  [localeId: string]: string;
}

export type TranslateableText = string | MultilingualText;

export function typeIsMultilingualText(value: any): value is MultilingualText {
  const localTest = /^[a-z]{2}$/;
  return (
    value instanceof Object &&
    typeof value.en === "string" &&
    Object.entries(value).every(([local, val]: [string, any]) => {
      return localTest.test(local) && typeof val === "string";
    })
  );
}

export function typeIsTranslateableText(
  value: any
): value is TranslateableText {
  return typeof value === "string" || typeIsMultilingualText(value);
}
export const tt2str = (text: TranslateableText, locale: string) => {
  if (typeof text === "string") {
    return text;
  } else if (text instanceof Object && typeof text[locale] === "string") {
    return text[locale];
  } else if (text instanceof Object && typeof text.en === "string") {
    return text.en;
  } else {
    console.warn("tt2str failed for", locale, text);
    return "";
  }
};
