export interface RootState {
  version: string;
  loadingMessage: null | string;
  [key: string]: any;
}

export enum VuexModulNames {
  AUDIT = "audit"
}

export enum CollectionNames {
  AUDITS = "audits",
  MEASURES = "measures",
  TEMPLATES = "templates",
  SCHEMAS = "schemas",
  CONFIGURATION = "configuration"
}

export enum KnownDocumentNames {
  AUDIT_CLASSES = "auditClasses"
}
