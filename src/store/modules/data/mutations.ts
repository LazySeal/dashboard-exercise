import { MutationTree } from "vuex";
import { DataState } from "./types";
// import Vue from "vue";

const SET_ROWS = "SET_ROWS";
const RESET_STATE = "RESET_STATE";

const n = {
  SET_ROWS,
  RESET_STATE
};

const mutations: MutationTree<DataState> = {
  [n.SET_ROWS](state, payload: any) {
    state.rows = payload;
  },
  [n.RESET_STATE](state) {
    state.rows = [];
  }
};

export { n as mutationNames, mutations };
