export class DataState {
  rows: {
    questionTypeId: string; //: "966f5460-5e25-55dd-9950-09775bb61cfa",
    questionTypeText: string; //: "Wichtiges Auditkriterium",
    questionTypeValue: number; //: 3,
    questionChapters: string; //: "ISO 9001: 2015 7.1, ISO 9001: 2015 7.1.2, ISO 9001: 2015 7.1.6",
    findingId: string; //: "-LWv0huKsHMZOPQruBW7",
    findingTypeId: string; //: "d90e2f59-08a6-5c39-b204-fa537f6bd1d1",
    findingTypeText: string; //: "Bemerkung",
    findingTypeValue: number; //: 0,
    auditId: string; //: "-LVgHHKpNTdBOTeu7sy2",
    auditPlanningYear: number; //: 2019,
    auditAuditingDate: string; //: "2019-01-22T00:00:00.000Z",
    auditStandards: string; //: "ISO 9001: 2015",
    auditStatus: string; //: "5",
    auditStatusDescription: string; //: "Wirksamkeitsprüfung abgeschlossen",
    auditCreateTime: string; //: "2019-03-21T21:48:48.749Z",
    auditLastUpdateTime: string; //: "2019-07-27T11:21:37.911Z"
    measureId: string; // "0b811c81-56f4-49b1-94b6-9ade59656756";
    measureMeasuretype: string; // "1";
    measureStatus: string; //"4";
    measureStatusDescription: string; // "Abgeschlossen";
    measureCreateTime: string; // "2019-04-10T11:49:55.831Z";
    measureDueDate: string; // "2019-05-31";
    measureLastUpdateTime: string; // "2019-07-10T12:20:22.754Z";
  }[] = [];
}
