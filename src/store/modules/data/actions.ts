import { ActionTree, ActionContext } from "vuex";
import { RootState } from "../../types";
import { DataState } from "./types";
import { mutationNames as mn } from "./mutations";
import { getterNames as gn } from "./getters";
import statistical_data from "./statistical_data.json";
// import { getterNs, logXStored, mutationNs, actionNs } from "@/utils/VuexHelper";

const loadData = "loadData";

const n = {
  loadData
};

const actions: ActionTree<DataState, RootState> = {
  async [n.loadData]({ rootState, commit, getters, dispatch }, payload: any) {
    commit(mn.SET_ROWS, statistical_data);
  }
};

export { n as actionNames, actions };
