import { GetterTree } from "vuex";
import { DataState } from "./types";
import { RootState } from "../../types";
// import _, { Dictionary } from "lodash";
// import { api } from "./index";

// import { getterNs, actionNs } from "@/utils/VuexHelper";
import { nullable } from "@/common/types";
// import { nullable, tt2str } from "../../../../common/types/common";

const getRandomData = "getRandomData";
const getMeasuresPerMonth = "getMeasuresPerMonth";
const getAvgProcessingTime = "getAvgProcessingTime";
const getMeasureTypeProportion = "getMeasureTypeProportion";
const getMeasuresProcessedAfterDueDate = "getMeasuresProcessedAfterDueDate";
const getFindingTypeProportion = "getFindingTypeProportion";

const n = {
  getRandomData,
  getMeasuresPerMonth,
  getAvgProcessingTime,
  getMeasureTypeProportion,
  getMeasuresProcessedAfterDueDate,
  getFindingTypeProportion
};

const monthNames = [
  "Januar",
  "Februar",
  "März",
  "April",
  "Mai",
  "Juni",
  "Juli",
  "August",
  "September",
  "Oktober",
  "November",
  "Dezember"
];

function getRandomInt() {
  return Math.floor(Math.random() * (50 - 5 + 1)) + 5;
}

function getMonth(dateString: string): string {
  const date = new Date(dateString);
  return date.toLocaleString("default", { month: "long" });
}

function getProcessingTime(startDate: string, endDate: string): number {
  const startTime = new Date(startDate).getTime();
  const endTime = new Date(endDate).getTime();
  const diff = endTime - startTime;

  return diff / (1000 * 60 * 60 * 24);
}

function getRandomColorHex(): string {
  return "#" + ((Math.random() * 0xffffff) << 0).toString(16).padStart(6, "0");
}

function isDueDateMissed(
  dueDateStr: string,
  lastUpdateStr: string,
  status: string
) {
  const dueDate = new Date(dueDateStr);
  const lastUpdate = new Date(lastUpdateStr);
  const now = new Date();

  // Due date is missed, if
  // 1. last update is later than due date, or
  // 2. status is not 4 and now is later than due date
  if (dueDate < lastUpdate || (+status !== 4 && dueDate < now)) {
    return true;
  }

  // Otherwise return false
  return false;
}

const getterTree: GetterTree<DataState, RootState> = {
  [n.getRandomData](state, getters, rootState, rootGetters): any {
    return {
      labels: [getRandomInt(), getRandomInt()],
      datasets: [
        {
          label: "Data One",
          backgroundColor: "#f87979",
          data: [getRandomInt(), getRandomInt()]
        },
        {
          label: "Data One",
          backgroundColor: "#f87979",
          data: [getRandomInt(), getRandomInt()]
        }
      ]
    };
  },
  [n.getMeasuresPerMonth](state, getters, rootState, rootGetters): any {
    const accMonth = {
      Januar: 0,
      Februar: 0,
      März: 0,
      April: 0,
      Mai: 0,
      Juni: 0,
      Juli: 0,
      August: 0,
      September: 0,
      Oktober: 0,
      November: 0,
      Dezember: 0
    };

    // 1. Iterate rows to find all datapoint that have a measureId
    for (const row of state.rows.values()) {
      if (row.measureId) {
        // Increase counter of respective month
        accMonth[getMonth(row.measureCreateTime)]++;
      }
    }

    // 2. Generate target dataset
    const data: Array<number> = [];

    for (const acc of Object.values(accMonth)) {
      data.push(acc);
    }

    // 3. Return target object
    return {
      labels: monthNames,
      datasets: [
        {
          label: "Anzahl der Maßnahmen",
          backgroundColor: getRandomColorHex(),
          data
        }
      ]
    };
  },
  [n.getAvgProcessingTime](state, getters, rootState, rootGetters): any {
    const accFindingTypes: Array<{
      findingTypeId: string;
      months: Array<{ month: string; processingTimes: number[] }>;
    }> = [];

    // 1. Iterate rows to find all datapoint that have a measureId and store created month and processing time per finding type
    for (const row of state.rows.values()) {
      if (row.measureId) {
        // Create new finding type entry if not seen before
        if (!accFindingTypes.find(x => x.findingTypeId === row.findingTypeId)) {
          // Create entry
          const findingType: {
            findingTypeId: string;
            months: Array<{ month: string; processingTimes: number[] }>;
          } = { findingTypeId: row.findingTypeId, months: [] };

          // Add months
          for (const month of monthNames) {
            findingType.months.push({
              month,
              processingTimes: []
            });
          }

          accFindingTypes.push(findingType);
        }

        // Add processing time for finding type to corresponding month
        accFindingTypes
          .find(x => x.findingTypeId === row.findingTypeId)
          ?.months.find(y => y.month === getMonth(row.measureCreateTime))
          ?.processingTimes.push(
            getProcessingTime(row.measureCreateTime, row.measureDueDate)
          );
      }
    }

    // Target dataset
    const datasets: Array<{
      label: string;
      backgroundColor: string;
      data: number[];
    }> = [];

    // 2. Iterate accMonths to calculate avg processing time per month per finding type id
    for (const [index, findingTypes] of Object.entries(accFindingTypes)) {
      // Dataset to be appended to datasets array
      const dataset = {
        label: `Finding type: ${index}`,
        backgroundColor: getRandomColorHex(),
        data: [] as number[]
      };

      for (const monthlyProcessingTimes of findingTypes.months) {
        // Calculate avg
        let sum = 0;
        for (const processingTime of monthlyProcessingTimes.processingTimes) {
          sum += processingTime;
        }
        const avg = sum / monthlyProcessingTimes.processingTimes.length;

        dataset.data.push(avg);
      }

      datasets.push(dataset);
    }

    return {
      labels: monthNames,
      datasets
    };
  },
  [n.getMeasureTypeProportion](state, getters, rootState, rootGetters): any {
    const accMeasureTypes: Array<{ measureTypeId: string; acc: number }> = [];

    for (const row of state.rows.values()) {
      if (row.measureId && row.measureMeasuretype) {
        const mt = accMeasureTypes.find(
          x => x.measureTypeId === row.measureMeasuretype.toString()
        );
        // Create new measure type entry if not seen before
        if (!mt) {
          accMeasureTypes.push({
            measureTypeId: row.measureMeasuretype,
            acc: 0
          });
        } else {
          // Increase acc, if measure type already initialised
          mt.acc++;
        }
      }
    }

    const labels: string[] = [];
    const data: number[] = [];
    const backgroundColor: string[] = [];

    for (const entry of accMeasureTypes) {
      labels.push(entry.measureTypeId);
      data.push(entry.acc);
      backgroundColor.push(getRandomColorHex());
    }

    return {
      labels,
      datasets: [{ data, backgroundColor }]
    };
  },
  [n.getMeasuresProcessedAfterDueDate](
    state,
    getters,
    rootState,
    rootGetters
  ): any {
    let countMeaserWithDueDateMissed = 0;
    let countMeaserWithDueDatePassed = 0;

    for (const row of state.rows.values()) {
      if (
        row.measureId &&
        isDueDateMissed(
          row.measureDueDate,
          row.measureLastUpdateTime,
          row.measureStatus
        )
      ) {
        countMeaserWithDueDateMissed++;
      } else {
        countMeaserWithDueDatePassed++;
      }
    }

    return {
      labels: ["Due date passed", "Due date missed"],
      datasets: [
        {
          data: [countMeaserWithDueDatePassed, countMeaserWithDueDateMissed],
          backgroundColor: [getRandomColorHex(), getRandomColorHex()]
        }
      ]
    };
  },
  [n.getFindingTypeProportion](state, getters, rootState, rootGetters): any {
    const accFindingTypes: Array<{
      findingTypeId: string;
      months: Array<{ month: string; acc: number }>;
    }> = [];

    for (const row of state.rows.values()) {
      if (row.findingTypeId) {
        // Create new finding type entry if not seen before
        if (
          !accFindingTypes.find(
            x => x.findingTypeId === row.findingTypeId.toString()
          )
        ) {
          // Create entry
          const findingType: {
            findingTypeId: string;
            months: Array<{ month: string; acc: number }>;
          } = { findingTypeId: row.findingTypeId, months: [] };

          // Add months
          for (const month of monthNames) {
            findingType.months.push({
              month,
              acc: 0
            });
          }

          accFindingTypes.push(findingType);
        }

        // Add processing time for finding type to corresponding month
        const entry = accFindingTypes
          .find(x => x.findingTypeId === row.findingTypeId)
          ?.months.find(y => y.month === getMonth(row.auditCreateTime));

        // Increase acc
        if (entry) entry.acc++;
      }
    }

    // Target dataset
    const datasets: Array<{
      label: string;
      backgroundColor: string;
      data: number[];
    }> = [];

    // 2. Iterate accMonths to calculate avg processing time per month per finding type id
    for (const [index, findingTypes] of Object.entries(accFindingTypes)) {
      // Dataset to be appended to datasets array
      const dataset = {
        label: `Finding type: ${index}`,
        backgroundColor: getRandomColorHex(),
        data: [] as number[]
      };

      for (const monthlyProportion of findingTypes.months) {
        dataset.data.push(monthlyProportion.acc);
      }

      datasets.push(dataset);
    }

    return {
      labels: monthNames,
      datasets
    };
  }
};

export { n as getterNames, getterTree as getters };
