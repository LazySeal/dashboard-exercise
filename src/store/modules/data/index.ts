import { Module } from "vuex";
import { DataState } from "./types";
import { RootState } from "../../types";

import { getters, getterNames } from "./getters";
import { actions, actionNames } from "./actions";
import { mutations, mutationNames } from "./mutations";

const state = new DataState();

const namespaced: boolean = true;
const modul: Module<DataState, RootState> = {
  namespaced,
  getters,
  actions,
  mutations,
  state
};

export const api = {
  namespace: "data",
  mutations: mutationNames,
  getters: getterNames,
  actions: actionNames
};
export default modul;
