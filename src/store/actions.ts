import { ActionTree } from "vuex";
import { RootState } from "./types";

import { api as auditApi } from "./modules/data";
import { mutationNs, actionNs } from "@/utils/VuexHelper";
// import { api } from "./index";

const unloadState = "unloadState";

const n = {
  unloadState
};

const actions: ActionTree<RootState, RootState> = {
  [n.unloadState]({ commit }) {
    console.log("UNLOAD STATE");
    commit(mutationNs(auditApi, auditApi.mutations.RESET_STATE));
  }
};

export { n as actionNames, actions };
