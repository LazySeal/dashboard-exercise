# audit-cloud-remote

# VS Code Setup

Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter.

```
ext install esbenp.prettier-vscode
```

```
ext install octref.vetur
```

```
ext install vuetifyjs.vuetify-vscode
```

```
ext install netcorext.uuid-generator
```

```
ext install alexdima.copy-relative-path
```

```
ext install editorconfig.editorconfig
```

```
ext install toba.vsfire
```

```
ext install jebbs.plantuml
```

```
ext install shd101wyy.markdown-preview-enhanced
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
